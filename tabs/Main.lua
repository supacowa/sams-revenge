-- project: sams-revenge
-- version: prototype
-- (c) August 2017 by kontakt@herrsch.de



displayMode(STANDARD)
--supportedOrientations(LANDSCAPE_ANY)


function setup()
    device_simulator = DeviceSimulator()
    device_simulator.is_running = false
    
    mesh_rigger = MeshRig()
    --mesh_rigger.is_running = false
end


function orientationChanged(screen)
    device_simulator:updateUserInterface()
end


function draw()
    background(127, 127, 127, 255)
    
    device_simulator:draw()
    mesh_rigger:draw()
    
    debugger(1, 0)
end



function touched(touch)
    mesh_rigger:touched(touch)
end
